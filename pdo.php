<?php  

// CÓDIGO COM AS PRINCIPAIS FUNÇÕES DE MANIPULÇÃO DE BANCO DE DADOS COM PDO.
//header('Content-Type: text/html; charset=iso-8859-1'); // Comando para não quebrar os caracters na hora de imprimir resutlados que contém acentuação

// EXEMPLO 0 : CONEXÃO
// CONECTANDO-SE A UM BANCO DE DADOS COM A FUNÇÃO TRY / CATCH PARA CAPTURAR EVENTUAIS ERROS!!
// try{
//     $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//     $conexao = NULL;
//         }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//     }

// EXEMPLO 1 : LISTAGEM NO FORMATO DE OBJETO
// LISTAGEM DAS INFORMAÇÕES DO BANCO DE DADOS EM FORMA DE OBJETOS.
// try{
// // $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
// $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
// var_dump($resultados); // MOSTRAR O TIPO RETORNADO, QUE SERÁ UM OBJETO.
// if($resultados) // Se a variavel $resultado estiver informações
// {
//     foreach ($resultados as $resultado)
//     {
//         /* Impressão dos dados das colunas empresa e contato da tabela
//          fornecedor. */
//         echo $resultado['nome']." | ". $resultado['email'] . "<br>";
//     }
// }
// $conexao = NULL; // Terminar a conexão
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }


// EXEMPLO 2 : LISTAGEM NO FORMATO DE ARRAY
// TRANSFORMAR O OBJETO RETORNADO EM UM ARRAY PARA PODER SER MANIPULADO
// try{
//     $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//     $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
//     $pessoas = $resultados->fetchAll(); // O MÉTODO FETCHALL() TRAZ TODAS AS OCORRENCIAS GRAVADAS NO BANCO DE DADOS.
//     echo "<pre>";
// //     var_dump($pessoas); // 
// //     var_dump($pessoas[0]['nome']); // SELECIONANDO UMA CELULA EM ESPECIFICO
// //     var_dump($pessoas[0]['email']); // SELECIONANDO UMA CELULA EM ESPECIFICO
// //     var_dump($pessoas[0][0]); // SELECIONANDO UMA CELULA EM ESPECIFICO
//     echo "</pre>";
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }



// EXEMPLO 3 : LISTAGEM NO FORMATO DE ARRAY COM MANIPULAÇÃO DO FOREACH
// MANIPULAÇÃO DO RESULTADO DA LISTAGEM POR MEIO DO CONTROLE DE FLUXO FOREACH()
// try{
//     $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//     $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
//     $pessoas = $resultados->fetchAll();
//     foreach($pessoas as $pessoa){
//         var_dump($pessoa['nome']." => ".$pessoa['email'].'<br>');
//     }
//     $conexao = NULL;
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }


// EXEMPLO 4: USANDO O METODO fetch() E OS MÉTODOS 
// MANIPULAÇÃO DAS CHAVES DOS ARRAYS POR MEIO DOS MÉTODOS DO PDO.
// TRY{
// $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
// $resultados = $conexao->query("SELECT nome,email FROM curso_pessoa");
// // $pessoas = $resultados->fetch(); // Traz apenas a primeira linha da coluna ordenado ordenadas por keys associativos numericos e string.
// // $pessoas = $resultados->fetch(PDO::FETCH_ASSOC); // Traz apenas a primeira linha da coluna com as keys associativos ordenados por string.
// // $pessoas = $resultados->fetch(PDO::FETCH_BOTH); // Traz apenas a primeira linha da coluna com as keys ordenado por nome das tabelas e numero de posiçao.
// // $pessoas = $resultados->fetch(PDO::FETCH_LAZY); // Traz apenas a primeira linha da coluna com as keys ordenado por string e posições numericas das tabelas e uma key com o query usado na busca.
// // $pessoas = $resultados->fetch(PDO::FETCH_NUM); // Traz apenas a primeira linha da coluna com as keys ordenadas pela posição numero.
// echo "<pre>";
// var_dump($pessoas);

// echo "</pre>";
// $conexao = NULL;
// }
// catch (PDOException $e){
//     print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
// }

// EXEMPLO 5: INSERINDO DADOS com ?
// O PARÂMENTRO '?' SERVE PARA INDICAR A VARIÁVEL QUE IRÁ FAZER PARTE DA LÓGICA DO SQL 
//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");

// O MÉTODO PREPRERE() IRÁ HABILITAR O SQL PARA SE USADO POSTERIORMENTE. PREVINE SQL INJECTION.
//         $sql = $conexao->prepare("INSERT INTO curso_pessoa (id,nome,email,celular,dataNascimento) VALUES(?,?,?,?,?)");

// A MÉTODO bindValue() LIGA OS MARCADORES '?' PRESENTES NO SQL AOS SEUS RESPECTVIVOS VALORES, PASSANDO AO METODO SUA POSIÇÃO E VALOR.
//         $sql->bindValue(1, 30);
//         $sql->bindValue(2, 'Fabricio');
//         $sql->bindValue(3, 'fabfab@gmail.com');
//         $sql->bindValue(4, '(67)87234567');
//         $sql->bindValue(5, '2011-02-04');
 
// O METODO EXECUTE() IRÁ EXECUTAR O SQL PARA QUE SEJA FEITA A AÇÃO NO BANCO DE DADOS.
//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//     }
    
 // EXEMPLO 6: INSERINDO DADOS COM :CHAVES
    
//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//         $sql = $conexao->prepare("INSERT INTO curso_pessoa (id,nome,email,celular,dataNascimento) VALUES(:id,:nome,:email,:celular,:dataNascimento)");
        
//         $sql->bindValue(':id', 31);
//         $sql->bindValue(':nome', 'Oracio');
//         $sql->bindValue(':email', 'orab@hotmail.com');
//         $sql->bindValue(':celular', '(67)8746537');
//         $sql->bindValue(':dataNascimento', '2013-06-08');
        
//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 7: ATUALIZAND DADOS COM :CHAVES

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = :email, celular = :celular WHERE id = :id");
        

//         $sql->bindValue(':id', 31);
//         $sql->bindValue(':email', 'orab500@hotmail.com');
//         $sql->bindValue(':celular', '(67)111111');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 9: ATUALIZAND DADOS COM ?

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = ?, celular = ? WHERE id = ?");


//         $sql->bindValue(3, 31);
//         $sql->bindValue(1, 'orab500@yahoo.com.br');
//         $sql->bindValue(2, '(67)11666');

//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }

// EXEMPLO 10: ATUALIZAND DADOS COM :CHAVES E ARRAY NO EXECUTE

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//         $sql = $conexao->prepare("UPDATE curso_pessoa SET email = :email, celular = :celular WHERE id = :id");

//         $sql->execute(array(':id'=>31,':email'=>'orab500@outlook.com',':celular'=>'(67)222222'));
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }


// EXEMPLO 11: DELETANDO DADOS 

//     try{
//         $conexao = new PDO("mysql:host=localhost;dbname=curso", "root", "");
//         $sql = $conexao->prepare("DELETE FROM curso_pessoa WHERE id = ?");

//         $sql->bindValue(1,31);
//         $sql->execute();
//         $conexao = NULL;
//     }
//     catch (PDOException $e){
//         print "Ops, algo deu errado: " . $e->getMessage(). "<br>";
//         die();
//     }

?>
  